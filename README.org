#+TITLE: =README=
#+AUTHOR: Haider Mirza
#+DESCRIPTION: A README for my website repository
* Table Of Contents :toc:
- [[#who-am-i][Who am I?]]
- [[#about-this-repository][About this repository]]

* Who am I?
#+begin_src
  _   _ __  ___
 | | | |  \/  |
 | |_| | |\/| | My Github: https://github.com/Ha1derMirza
 |  _  | |  | | My Matrix: @haider.mirza:matrix.org
 |_| |_|_|  |_|
#+end_src
My Name is Haider Mirza and I am a 3D artist and Programmer.
All my projects are hosted on github and are free and open-source.
Most of my projects are licensed under GPL V3.

* About this repository
This is the Repository where I keep and control Github pages.
Like all of my projects this file is also Open Source (under GPL v3).
